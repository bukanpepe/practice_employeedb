from flask import Flask, jsonify, request
from sqlalchemy import create_engine
from sqlalchemy.sql import text

app = Flask(__name__)

conn_str = 'postgresql://postgres:Putranurelva2804?@localhost:5000/practice_employeeDB'
engine = create_engine(conn_str, echo=False)

@app.route('/index', methods=['GET'])
def greetings():
    return "Hello"
    
@app.route('/employee', methods=['GET'])
def allEmployee():
    all = []
    get_param = request.args.get("sort_by")
    try :
        with engine.connect() as connection:
            if get_param == None or get_param == '':
                qry = text("SELECT * FROM employee")
                result = connection.execute(qry)
                for row in result:
                    all.append({"nik": row[0], "name" : row[1], "start_year": row[2]})
                return jsonify(all)
            else:
                qry = text(f"SELECT * FROM employee ORDER BY {get_param}")
                result = connection.execute(qry)
                for row in result:
                    all.append({"nik": row[0], "name" : row[1], "start_year": row[2]})
                return jsonify(all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/employee/add', methods=['POST'])
def add_employee():
    data = []
    body = request.json
    _nik = body.get('NIK')
    _name = body.get('Name')
    _start_year = int(body.get('Year'))

    try:
        with engine.connect() as connection:
            qry = text("INSERT INTO public.employee(nik, name, start_year) VALUES (:new_nik, :new_name, :new_start_year)")
            result = connection.execute(qry, new_nik=_nik, new_name=_name, new_start_year=_start_year)
            new = text("SELECT * FROM employee ORDER BY start_year")
            get_all = connection.execute(new)
            for row in get_all:
                data.append({"nik" : row[0], "name" : row[1], "start_year" : row[2]})
            return jsonify(data)
    except Exception as e:
        return jsonify(str(e))
@app.route('/employee/edit', methods=['PUT'])
def edit_employee():
    data = []
    body = request.json
    _nik = body.get("NIK")
    _name = body.get("Name")
    _year = body.get("Year")
    try:
        with engine.connect() as connection:
            qry = text("UPDATE employee SET name=:name, start_year=:year WHERE nik=:nik")
            result = connection.execute(qry, name=_name, year=_year, nik=_nik)
            new = text("SELECT * FROM employee ORDER BY nik")
            get_all = connection.execute(new)
            for row in get_all:
                data.append({"nik" : row[0], "name" : row[1], "start_year" : row[2]})
            return jsonify(data)
    except Exception as e:
        return jsonify(error=str(e))
@app.route('employee/remove', methods=['DELETE'])
def remove_employee():
    data = []
    body = request.json
    _nik = body.get("NIK")
    try:
        with engine.connect() as connection:
            qry = text("DELETE FROM employee WHERE nik=:nik")
            result = connection.execute(qry)
            new = text("SELECT * FROM employee ORDER BY nik")
            get_all = connection.execute(new)
            for row in get_all:
                data.append({"nik" : row[0], "name" : row[1], "start_year" : row[2]})
            return jsonify(data)

########## LEAVE ##########

@app.route('/employee/leave', methods=['GET'])
def get_all_leave():
    all = []
    start = request.args.get('start_date')
    stop = request.args.get('end_date')
    try:
        with engine.connect() as connection:
            if start == None and stop == None or start == '' and stop == '': 
                qry = text("SELECT employee.name, employee.start_year, leave.start_date, leave.end_date, leave.end_date-leave.start_date FROM employee JOIN leave ON(employee.nik = leave.employee_nik)")
                result = connection.execute(qry)
                for row in result:
                    all.append({"name" : row[0], "start_year": row[1], "leave_start" : row[2], "leave_stop" : row[3], "total days" : row[4].days + 1})
                return jsonify(all)
            elif stop == None or stop == '':
                qry = text("SELECT employee.name, employee.start_year, leave.start_date, leave.end_date, leave.end_date-leave.start_date FROM employee JOIN leave ON(employee.nik = leave.employee_nik) WHERE leave.start_date>=:start_date")
                result = connection.execute(qry, start_date=start)
                for row in result:
                    all.append({"name" : row[0], "start_year": row[1], "leave_start" : row[2], "leave_stop" : row[3], "total days" : row[4].days + 1})
                return jsonify(all)
            else:
                qry = text("SELECT employee.name, employee.start_year, leave.start_date, leave.end_date, leave.end_date-leave.start_date FROM employee JOIN leave ON(employee.nik = leave.employee_nik) WHERE leave.start_date>=:start_date AND leave.end_date<=:end_date")
                result = connection.execute(qry, start_date=start, end_date=stop)
                for row in result:
                    all.append({"name" : row[0], "start_year": row[1], "leave_start" : row[2], "leave_stop" : row[3], "total days" : row[4].days + 1})
                return jsonify(all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/employee/leave/add', methods=['POST'])
def add_leave():
    data = []
    body = request.json
    _nik = body.get("NIK")
    _start = body.get("start_date")
    _stop = body.get("end_date")
    try:
        if _start == None or _stop == None or _start =='' or _stop =='':
            return jsonify(error="Please enter a valid 'yyyy-mm-dd' date format")
        else:
            qry = text("INSERT INTO public.leave(employee_nik, start_date, end_date) VALUES(:nik, :start_date, :end_date)")
            result = connection.execute(qry, nik=_nik, start_date=_start, end_date=_stop)
            new = text("SELECT employee.name, employee.start_year, leave.start_date, leave.end_date, leave.end_date-leave.start_date FROM employee JOIN leave ON(employee.nik = leave.employee_nik)")
            get_all = connection.execute(new)
            for row in result:
                all.append({"name" : row[0], "start_year": row[1], "leave_start" : row[2], "leave_stop" : row[3], "total days" : row[4].days + 1})
            return jsonify(all)
    except Exception as e:
        return jsonify(error=str(e))

if __name__ == '__main__':
    app.run()